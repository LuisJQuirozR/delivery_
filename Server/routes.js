let express = require('express');
let router = express.Router();

console.log("cargando controller...");
let ctrl = require('./controller.js');

//API

//router.post('/embarques', ctrl.ConsultaEmbarquesAnoMes);

router.post('/json', ctrl.json);

router.get('/test', ctrl.test);

//
//router.get('/',ctrl.index);

//usuarios

router.get('/usuarios', ctrl.usuarios);

router.get('/usuario', ctrl.buscarUsuario);

router.post('/usuario', ctrl.crearUsuario);

router.put('/usuario', ctrl.actualizarUsuario);

router.delete('/usuario', ctrl.eliminarUsuario);

router.get('/usuario/auth', ctrl.autenticarUsuario);

module.exports = router;
