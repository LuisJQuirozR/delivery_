angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('loginUi', {
    url: '/login',
    templateUrl: 'templates/loginUi.html',
    controller: 'loginUiCtrl'
  })

  .state('customer_DirectionUI', {
    url: '/direcciones',
    templateUrl: 'templates/customer_DirectionUI.html',
    controller: 'customer_DirectionUICtrl'
  })

  .state('customerUi', {
    url: '/Customer',
    templateUrl: 'templates/customerUi.html',
    controller: 'customerUiCtrl'
  })

  .state('partners_CategoryUI', {
    url: '/Category',
    templateUrl: 'templates/partners_CategoryUI.html',
    controller: 'partners_CategoryUICtrl'
  })

  .state('main', {
    url: '/page10',
    templateUrl: 'templates/main.html',
    controller: 'mainCtrl'
  })

  .state('produtc_List', {
    url: '/page11',
    templateUrl: 'templates/produtc_List.html',
    controller: 'produtc_ListCtrl'
  })

  .state('product_Details', {
    url: '/page12',
    templateUrl: 'templates/product_Details.html',
    controller: 'product_DetailsCtrl'
  })

  .state('orders_Details', {
    url: '/page13',
    templateUrl: 'templates/orders_Details.html',
    controller: 'orders_DetailsCtrl'
  })

  .state('profile_Details', {
    url: '/page14',
    templateUrl: 'templates/profile_Details.html',
    controller: 'profile_DetailsCtrl'
  })

  .state('signup', {
    url: '/page15',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

$urlRouterProvider.otherwise('/#/login');


});
