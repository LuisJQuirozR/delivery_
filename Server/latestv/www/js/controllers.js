angular.module('app.controllers', [])

    .controller('clientesCtrl', ['$scope', '$stateParams', '$state','$http',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $state, $http) {

            console.log("from clientesCtrl");

             //$scope.listaclientes = [{nombre:"alberto"},{nombre:"jesus"}];
            $scope.listaclientes = [];

            $scope.init = function () {
                console.log("from init");
                $http({
                    method: "GET",
                    url: "http://localhost:3000/usuarios"
                }).then(function mySuccess(response) {
                    console.log(response.data.recordset);
                    $scope.listaclientes = response.data.recordset;
                    //$scope.listaclientes = [{ nombre: "alberto" }, { nombre: "jesus" }];

                }, function myError(response) {
                    $scope.myWelcome = response.statusText;
                }); 
            }

            $scope.init();
            
        }])

    
.controller('sideMenuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('loginUiCtrl', ['$scope', '$stateParams', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

  $scope.user = {
        email : "abc",
        password : "123"
    };

  $scope.autenticar= function(){

    if( ($scope.user.email == "abc") && ($scope.user.password=="123") )
    {
      $state.go('partners_CategoryUI',{});
    }
    else
    {
      alert("Usuario invalido");
    }
  }
  
    $scope.goclientes = function () {
        console.log("from clientes() in loginUICtrl");
        $state.go('clientes', {}); 
    }

 
}])

/*
$scope.test = function(){
{
  $http({method: "GET", url: "http://localhost:3000/api/usuario/autenticar", data:{user: "agarcia", password: "123"}).
      success(function(data, status) {
        $scope.status = status;
        $scope.data = data;
        alert(JSON.stringify(data));
      }).
      error(function(data, status) {
        $scope.data = data || "Request failed";
        $scope.status = status;
    });
}
//>>>>>>> 8e3afe88fadd29deb4a34b93147be72de71e83ea


}])
*/

.controller('customer_DirectionUICtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('customerUiCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])



.controller('partners_DetailUICtrl', ['$scope', '$stateParams', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

  //console.log($stateParams);

  $scope.detalle = $stateParams.detalle;

  //FILTRO PRODUCTOS DEL PARTNER
  $scope.productfilter = function(partid){
    //PARTNER ID
    //alert(partid);
    $state.go('produtc_List',{partnerid:partid});
  }

}])

.controller('partners_CategoryUICtrl', ['$scope', '$stateParams', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

  $scope.datos={
    idcategoria:0
  };

  $scope.categories = [
    {
        "ID": "1",
        "Nombre": "Comida"
    },
    {
        "ID": "2",
        "Nombre": "Farmacia"
    },
    {
        "ID": "3",
        "Nombre": "Bebidas"
    },{
      "ID": "4",
      "Nombre": "Licores"
  }
  ]

    $scope.Partners = [
      {
          "ID": "1",
          "Nombre": "McDonalds",
          "Categories":1
      },
      {
          "ID": "2",
          "Nombre": "KFC",
          "Categories":1
      },
      {
          "ID": "3",
          "Nombre": "Arturo",
          "Categories":1

      },
      {
        "ID": "4",
        "Nombre": "Farmatodo",
        "Categories":2
    },
    {
      "ID": "5",
      "Nombre": "Locatel",
      "Categories":2
  }] ;

$scope.detalle = [];



//filtro por cat
$scope.filtroporcat = function(id)
{
  console.log(id);
  $scope.datos.idcategoria=id;
  $scope.detalle = [];
  $scope.detalle = $scope.Partners.filter(e =>{
    return e.Categories==id;
  });
  console.log($scope.detalle);
  $state.go('partners_DetailUI',{detalle:$scope.detalle});

}


}])

.controller('mainCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('produtc_ListCtrl', ['$scope', '$stateParams','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

  console.log($stateParams.partnerid);

  $scope.partnerid = $stateParams.partnerid;

  $scope.products  = [
    {
        "ID": "1",
        "Nombre": "Hamburguesa",
        "Partner": "1"
    },
    {
      "ID": "2",
        "Nombre": "McPapas",
        "Partner": "1"
    },
    {
      "ID": "3",
      "Nombre": "Pollo",
      "Partner": "2"
    },
    {
    "ID": "4",
    "Nombre": "Maspollo",
    "Partner": "2"
    },
    {
      "ID": "5",
        "Nombre": "Combo1",
        "Partner": "3"
    },
    {
      "ID": "6",
      "Nombre": "Combo2",
      "Partner": "3"
    },
  ];

  $scope.productos = [];

  $scope.productos = $scope.products.filter(e =>{
    return e.ID==$scope.partnerid;
  });

  console.log($scope.productos);

  //VER detalle
  $scope.verdetalle = function(productid){

    $scope.producto = $scope.products.filter(e =>{
      return e.ID==$scope.productid;
    });

    $state.go('product_Details',{productid:$scope.productid});
  }

  //$state.go('partners_DetailUI',{detalle:$scope.detalle});

}])

.controller('product_DetailsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

  $scope.products  = [
    {
        "ID": "1",
        "Descripcion": "Hamburguesa con papas y refresco",
        "Price": "700"
    },
    {
      "ID": "2",
      "Descripcion": "Racion de papas fritas",
      "Price": "100"
    },
    {
      "ID": "3",
      "Descripcion": "6 Piezas de pollo",
      "Price": "500"
    },
    {
      "ID": "4",
      "Descripcion": "Racion de papas fritas",
      "Price": "100"
    },
    {
      "ID": "5",
      "Descripcion": "Combo 1",
      "Price": "200"
    },
    {
      "ID": "6",
      "Descripcion": "Combo 2",
      "Price": "300"
    },
    ]
    ;

}])

.controller('orders_DetailsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {




}])

.controller('profile_DetailsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('signupCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
