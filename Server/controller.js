const sql = require('mssql');
var fs = require('fs');

let dbConfig = {
    server: "DESKTOP-OCPR0UU",
    database: "delivery",
    user: "sa",
    password: "abc123",
    port: 1433
};

let tb = "dbo.usuarios";

let sp = "dbo.spUsuarios";

//prueba
exports.test = function (req, res) {

        console.log("Servidor respondiendo...");

        res.json({ message: "Respuesta desde prueba..." });
};


//INDEX
/*
exports.index = function(req, res)
{
    //res.sendFile(__dirname + "/index.html");
    res.sendfile('./public/client/index.html');
};
*/

//OK
exports.x = function(req, res)
{
    let pAno = req.body.ano;

    let pMes = req.body.mes;

    let pUsuario = req.body.usuario;

    console.log(pAno);

    console.log(pMes);

    console.log(pUsuario);

    //

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.input('Año', sql.Int, pAno)
        req.input('Mes', sql.Int, pMes)
        req.input('Usuario', sql.VarChar(50), pUsuario)
        .execute(sp).then(function (recordSet) {
            //console.log(recordSet.recordset);
            res.json( {recordset: recordSet.recordset} );
            conn.close();
        }).catch(function (err) {
            console.log(err);
            console.log("Error. Connection Closed!");
            conn.close();
        });
    }).catch(function (err) {
            console.log("Can't connect to db.");
            conn.close();
        console.log(err);
    });

};

exports.json = function (req, res) {

    let name = req.body.name;

    let edad = req.body.edad;

    console.log(name);

    console.log(edad);

    console.log("HI! " + name);

    res.json({ name: name, edad: edad });
};

//----------------USUARIOS --------------------//

//GET
exports.usuarios = function (req, res) {

    //let pAno = req.body.ano;

    //let pMes = req.body.mes;

    //let pUsuario = req.body.usuario;

    //console.log(pAno);

    //console.log(pMes);

    //console.log(pUsuario);

    //

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.execute("spUsuarios").then(function (recordSet) {
                res.json({ recordset: recordSet.recordset });
                conn.close();
            }).catch(function (err) {
                console.log(err);
                console.log("Error. Connection Closed!");
                conn.close();
            });
    }).catch(function (err) {
        console.log("Can't connect to db.");
        conn.close();
        console.log(err);
    });

};

//GET   Buscar Usuario
exports.buscarUsuario = function (req, res) {

    let pId = req.body.id;

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.input('id', sql.VarChar(50), pId)
            .execute("spBuscarUsuario").then(function (recordSet) {
            res.json({ recordset: recordSet.recordset });
            conn.close();
        }).catch(function (err) {
            console.log(err);
            console.log("Error. Connection Closed!");
            conn.close();
        });
    }).catch(function (err) {
        console.log("Can't connect to db.");
        conn.close();
        console.log(err);
    });
};

//POST
exports.crearUsuario = function (req, res) {

    let pNombre = req.body.nombre;
    let pUsuario = req.body.usuario;
    let pClave = req.body.clave;

    let pDireccion = req.body.direccion;
    let pTelefono = req.body.telefono;
    let pEmail = req.body.email;

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.input('nombre', sql.VarChar(50), pNombre)
            .input('usuario', sql.VarChar(50), pUsuario)
            .input('clave', sql.VarChar(50), pClave)
            .input('direccion', sql.VarChar(50), pDireccion)
            .input('telefono', sql.VarChar(50), pTelefono)
            .input('email', sql.VarChar(50), pEmail)
            .execute("spCrearUsuario").then(function (recordSet) {
                res.json({ message: "Usuario Creado" });
                conn.close();
            }).catch(function (err) {
                console.log(err);
                conn.close();
            });
        }).catch(function (err) {
            console.log("Can't connect to db.");
            conn.close();
        });
}

//PUT
exports.actualizarUsuario = function (req, res) {

    let pNombre = req.body.nombre;
    let pUsuario = req.body.usuario;
    let pClave = req.body.clave;

    let pDireccion = req.body.direccion;
    let pTelefono = req.body.telefono;
    let pEmail = req.body.email;
    let pId = req.body.id;

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.input('nombre', sql.VarChar(50), pNombre)
            .input('usuario', sql.VarChar(50), pUsuario)
            .input('clave', sql.VarChar(50), pClave)
            .input('direccion', sql.VarChar(50), pDireccion)
            .input('telefono', sql.VarChar(50), pTelefono)
            .input('email', sql.VarChar(50), pEmail)
            .input('id', sql.VarChar(50), pId)
            .execute("spActualizarUsuario").then(function (recordSet) {
                res.json({ message: "Usuario Actualizado" });
                conn.close();
            }).catch(function (err) {
                console.log(err);
                conn.close();
            });
    }).catch(function (err) {
        console.log("Can't connect to db.");
        conn.close();
    });
}

//DELETE
exports.eliminarUsuario = function (req, res) {

    let pId = req.body.id;

    console.log(pId);

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.input('id', sql.Int, pId)
            .execute("spEliminarUsuario").then(function (recordSet) {
                res.json({ message: "Usuario Eliminado" });
                conn.close();
            }).catch(function (err) {
                console.log(err);
                conn.close();
            });
    }).catch(function (err) {
        console.log("Can't connect to db.");
        conn.close();
    });
}

//GET AUTENTICAR USUARIO
exports.autenticarUsuario = function (req, res) {

    let pUser = req.body.user;

    let pPassword = req.body.password;

    console.log(pUser);

    console.log(pPassword);

    var conn = new sql.ConnectionPool(dbConfig);

    conn.connect().then(function () {

        console.log("Conectado...");

        var req = new sql.Request(conn);

        req.input('user', sql.VarChar(50), pUser)
            .input('password', sql.VarChar(50), pPassword)
            .execute("spAutenticarUsuario").then(function (recordSet) {
                //console.log(recordSet.recordset.length);
                if (recordSet.recordset.length==0)
                    res.json({ autenticado: 0, mensaje: "Datos Incorrectos o No Registrado." });
                else
                    res.json({ autenticado: 1 });
                conn.close();
            }).catch(function (err) {
                console.log(err);
                console.log("Error. Connection Closed!");
                conn.close();
            });
    }).catch(function (err) {
        console.log("Can't connect to db.");
        conn.close();
        console.log(err);
    });
};
