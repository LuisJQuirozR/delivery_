// Delivery - Node Server

var express = require('express');
var app = express();
const parse = require('querystring');

const sql = require('mssql');

var http = require('http');
var url = require('url');
var fs = require('fs');

var bodyParser = require('body-parser');
 var path = require('path');
//app.use(express.static(__dirname + '/public'));
//app.use(express.static('public'));
//app.use(express.static('public/client'));
//app.use('/static', express.static(__dirname + '/public'));
app.use(express.static(path.join(__dirname,"public")));

 console.log(__dirname);

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use( function(req, res, next) {

  if (req.originalUrl && req.originalUrl.split("/").pop() === 'favicon.ico') {
    return res.sendStatus(204);
  }

  return next();

});

//ROUTES
let rutas = require('./routes.js');
//app.use('/api', rutas);
//console.log("antes de cargar rutas...");
app.use('/', rutas);


//START SERVER
app.listen(3000, function () {
  console.log('Listening on port 3000!');
});
